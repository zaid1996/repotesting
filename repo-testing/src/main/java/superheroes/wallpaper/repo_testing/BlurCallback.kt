package superheroes.wallpaper.repo_testing

import android.view.View

interface BlurCallback {
    fun blurSelectedArea(left: Int, top: Int, right: Int, bottom: Int)
}