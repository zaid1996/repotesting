package superheroes.wallpaper.imageblur

import android.view.View

interface BlurCallback {
    fun blurSelectedArea(left: Int, top: Int, right: Int, bottom: Int)
}