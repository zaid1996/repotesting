package superheroes.wallpaper.imageblur

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.fivehundredpx.android.blur.BlurringView
import com.github.mmin18.widget.RealtimeBlurView
import java.util.*


class MainActivity : AppCompatActivity(), BlurCallback {

//    private var drawView: View=findViewById(R.id.asd)
    private lateinit var imageView: ImageView
    private lateinit var mBlurringView: BlurringView
    private lateinit var blurredView: View
    private lateinit var drawView: DrawView
    private  var blurViewsStack: Stack<View> = Stack()
    private lateinit var insertPoint: ViewGroup


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawView = findViewById(R.id.drawview)
        drawView.setCalback(this)
        blurredView = findViewById(R.id.blurred_view)

//        val blurDrawable =
//        blurDrawable.mixColor(Color.argb(99, 255, 255, 255))
//        findViewById(R.id.test_view).setBackground(blurDrawable)


    }



    override fun blurSelectedArea(left: Int, top: Int, right: Int, bottom: Int) {
//        Toast.makeText(this@MainActivity, "Donee", Toast.LENGTH_LONG).show()

//        val vi =
//            applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        val v: View = vi.inflate(R.layout.blur_view, null)

        var view = RealtimeBlurView(this,null)
        // fill in any details dynamically here
//        val blurringView: RealtimeBlurView = v.findViewById(R.id.blura)
//        blurringView.setBlurredView(blurredView)
//        v.makeDragAble(left.toFloat(),top.toFloat())


        blurViewsStack.push(view)



        // insert into main view
        insertPoint = findViewById(R.id.blur_test)
        insertPoint.addView(
            view,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        )
        var  param: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(right-left,bottom-top);
        param.setMargins(left,top,0,0)
        view.layoutParams = param
        view.makeDragAble()





    }


    fun View.makeDragAble() {
        var dX = this.x ; var  dY = this.y
        this.setOnTouchListener { p0, p1 ->
            when (p1!!.actionMasked) {
                MotionEvent.ACTION_DOWN -> {
                    dX = p0!!.x - p1.rawX
                    dY = p0.y - p1.rawY
                }
                MotionEvent.ACTION_MOVE -> {
                    p0!!.y = p1.rawY + dY
                    p0.x = p1.rawX + dX
                }
            }
            true
        }
    }

    fun deleteLastBlur(view: View){
        if(!blurViewsStack.empty()) {
            var v: View = blurViewsStack.pop()
            insertPoint.removeView(v)
        }
    }

}
